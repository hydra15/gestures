import 'package:flutter/material.dart';
import 'package:gestures/components/cbuttond.dart';
import 'package:gestures/components/cscreen.dart';

class CMenu extends StatelessWidget {
  const CMenu({
    Key? key,
    required this.title,
  }) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      // title: Text(title),
      // title with plus icon
      title: Row(
        children: [
          Text(title),
          // const Icon(Icons.add),
          // const SizedBox(width: 10),
          // icon to end rigth
          const Spacer(),
          // const Icon(Icons.person_add_alt_rounded),
          // make icon red color
          const Icon(Icons.person_add_alt_rounded, color: Colors.red),
        ],
      ),
      // content: const Text('La verificación será:'),
      // content with 4 cbutton

      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              // make button CButtond with onTap=_increment
              CButtond(
                text: 'Option_00',
                onTap: () {
                  Navigator.of(context).pop();
                  // call CScreen
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const CScreen(),
                    ),
                  );
                },
              ),
              CButtond(
                text: 'Option_01',
                onTap: () {
                  Navigator.of(context).pop();
                  // ignore: avoid_print
                  print('Option_01');
                },
              ),
              CButtond(
                text: 'Option_02',
                onTap: () {
                  Navigator.of(context).pop();
                  // ignore: avoid_print
                  print('Option_02');
                },
              ),
              CButtond(
                text: 'Option_03',
                onTap: () {
                  Navigator.of(context).pop();
                  // ignore: avoid_print
                  print('Option_03');
                },
              ),
            ],
          ),
        ],
      ),
      // add icon to title
      titlePadding: const EdgeInsets.fromLTRB(24.0, 24.0, 24.0, 0.0),
      titleTextStyle: const TextStyle(
        color: Colors.red,
        fontSize: 24.0,
        fontWeight: FontWeight.w700,
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text('Close'),
        ),
      ],
    );
  }
}
