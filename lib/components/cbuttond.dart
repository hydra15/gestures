// button with receive text and onTap
import 'package:flutter/material.dart';

class CButtond extends StatelessWidget {
  // text
  final String text;
  // onTap
  final VoidCallback onTap;

  const CButtond({Key? key, required this.text, required this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.all(8.0),
        // child: Text(
        //   text,
        //   style: const TextStyle(color: Colors.black),
        // ),
        // make text and icon
        child: Row(
          children: [
            Text(
              text,
              style: const TextStyle(color: Colors.black),
            ),
            const Spacer(),
            const Icon(Icons.arrow_forward_ios),
          ],
        ),
      ),
    );
  }
}
