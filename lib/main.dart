import 'package:flutter/material.dart';
import 'package:gestures/components/cbutton.dart';
import 'package:gestures/components/cmenu.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _decrementCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      // floatingActionButton: GestureDetector(
      //   onTap: _incrementCounter,
      //   onLongPress: _decrementCounter,
      //   child: const CButton(
      //     text: 'Increment',
      //   ),
      // ),
      // add 2 buttons
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: _incrementCounter,
            // onLongPress: _decrementCounter,
            // call CMenu
            onLongPress: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return const CMenu(
                    title: 'Menu',
                  );
                },
              );
            },
            child: const CButton(
              text: 'Increment',
            ),
          ),
          GestureDetector(
            onTap: _incrementCounter,
            onLongPress: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return const CMenu(
                    title: 'Menu',
                  );
                },
              );
            },
            child: const CButton(
              text: 'Decrement',
            ),
          ),
        ],
      ),
    );
  }
}
